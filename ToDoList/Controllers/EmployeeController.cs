﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoList.IServices;
using ToDoList.Models;

namespace ToDoList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService employeeService;
        public EmployeeController(IEmployeeService employee)
        {
            employeeService = employee;
        } 
       
        [HttpGet]
        [Route("[action]")]
        [Route("api/Employee/GetEmployee")]
       public IEnumerable<Employee> GetEmployee()
        {
            return employeeService.GetEmployee();
        }
        [HttpPost]
        [Route("[action]")]
        [Route("api/Employee/AddEmployee")]
        public Employee AddEmployee(Employee employee)       
        {
            return employeeService.AddEmployee(employee);
        }

        [HttpPut]
        [Route("[action]")]
        [Route("api/Employee/UpdateEmployee")]
        public Employee UpdateEmployee(Employee employee)
        {
            return employeeService.UpdateEmployee(employee);
        }

        [HttpDelete]
        [Route("[action]")]
        [Route("api/Employee/DeleteEmployee")]
        public Employee DeleteEmployee(int id)
        {
            return employeeService.DeleteEmployee(id);
        }

        [HttpGet]
        [Route("[action]")]
        [Route("api/Employee/GetEmployeeById")]
        public Employee GetEmployeeById(int id)
        {
            return employeeService.GetEmployeeById(id);
        }
    }
}
